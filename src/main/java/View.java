import java.util.List;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.ChatAction;
import com.pengrad.telegrambot.request.GetUpdates;
import com.pengrad.telegrambot.request.SendChatAction;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.BaseResponse;
import com.pengrad.telegrambot.response.GetUpdatesResponse;
import com.pengrad.telegrambot.response.SendResponse;

public class View implements Observer{

	
	TelegramBot bot = TelegramBotAdapter.build("570865421:AAEc2tZ0plA3kq1dq9NIvXclJAtTetWN0To");

	
	GetUpdatesResponse updatesResponse;
	
	SendResponse sendResponse;
	
	BaseResponse baseResponse;
			
	
	int queuesIndex=0;
	
	ControllerProcura controllerProcura; 
	
	boolean searchBehaviour = false;
	
	private Model model;
	
	public View(Model model){
		this.model = model; 
	}
	
	public void setControllerProcura(ControllerProcura controllerProcura){ 
		this.controllerProcura = controllerProcura;
	}
	
	public void receiveUsersMessages() {

		
		
		
		while (true){
		
			
			updatesResponse =  bot.execute(new GetUpdates().limit(100).offset(queuesIndex));
			
			
			List<Update> updates = updatesResponse.updates();

			
			for (Update update : updates) {
				
				
				queuesIndex = update.updateId()+1;
				
				if(this.searchBehaviour==true){
					this.callController(update);
					
				}else if(update.message().text().equals("jogo")){
					setControllerProcura(new ControllerProcuraJogador(model, this));
					sendResponse = bot.execute(new SendMessage(update.message().chat().id(),"qual jogo voc� procura?"));
					this.searchBehaviour = true;
					
					
				} else {
					sendResponse = bot.execute(new SendMessage(update.message().chat().id(),"Digite jogo para buscar por jogo"));
				}
				
				
				
			}

		}
		
		
	}
	
	
	public void callController(Update update){
		this.controllerProcura.search(update);
	}
	
	public void update(long chatId, String playerData){
		sendResponse = bot.execute(new SendMessage(chatId, playerData));
		this.searchBehaviour = false;
	}
	
	public void sendTypingMessage(Update update){
		baseResponse = bot.execute(new SendChatAction(update.message().chat().id(), ChatAction.typing.name()));
	}
	

}
